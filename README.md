# Superb-Feeds

* * Superb-Feed RSS retriever. * *

* Copyright (c) 2020 - Jim S. Smith, AFWS
* Licensed under GPL 2.0 or newer.


PURPOSE:

An easy way to incorporate RSS feeds into your site!


HOW TO SET UP / INSTALL:

All that is needed:

* Create a folder in your webroot folder. Can be anything like 'feeds', 'rss', etc.

* Copy the files (to include the '.include' folder into it.

* And then set up your 'my-feeds.txt' with some RSS feeds-links (one per line) in it.


HOW TO USE:

Then,

Simply accessing the folder (whether through an 'iframe' element, or 'include' the 'feeds/index.php' into your template or framework coding, and you're set!

Because many browsers have decided to drop support for RSS feeds (namely, no longer properly rendering the XML content), this module simply fetches the results, itself - and displays them, rendered in HTML to the visiting browser.

Article-featured images or the first posted image in a retrieved post will be displayed along with the post "gist".


REQUIRES:

* PHP SimpleXML library be installed and active.

* PHP minimum version of 5.6, but 7.2 or greater is recommended.

