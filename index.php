<?php	//	AFWS "Superb-Feed" Custom Feed Aggregator, (c)2020 AFWS

//	Name of custom RSS Feeds list file.
$feeds_file = "my-feeds.txt";

//	How many of the most-recent items to display from each channel?
$list_length = 5;

//	How many characters (length) to display of feed "gist"?
$gist_size = 100;

//	How many other site RSS feeds to display?
$channels = 5;

//	Disallow HTML in feeds? (Default action is: "true", if not defined.)
define( 'NO_HTML', true );

//	Set up load my CSP Headers module.
defined( 'DOC_ROOT' ) || define( 'DOC_ROOT', realpath( $_SERVER['DOCUMENT_ROOT'] ) );
require_once DOC_ROOT . '/.include/_security-headers.php';


$incl = __DIR__ . "/.include";

require_once "$incl/RSS-reader.php";

echo <<<HTML
<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="device-width">

	<title>RSS Feeds List.</title>

	<!-- Superb-Feed Aggregator, (c)2020 AFWS -->
	<style type="text/css">
		.content {background-color:#ddf;padding:5px}
		.post {background-color:#edc;padding:2px}
		.rss-site-title img {width:24px}
		.post-head img {max-width:550px;min-width:220px;width:97.55%}

		.rss-error {color:#e00}
	</style>

</head>

<body>

HTML;

if ( ! function_exists( 'simplexml_load_file' ) ) {
	echo "<h2 class='rss-error'>The PHP SimpleXML library needs to be installed and enabled!</h2>";
	}

else {
//	Does the feeds list file exist? If so, let's use its contents.
	if ( file_exists( "$incl/$feeds_file" ) && is_readable( "$incl/$feeds_file" ) ) {
		$url = trim( file_get_contents( "$incl/$feeds_file" ) );
		$url = preg_replace( array( "/((\r\n)+)/", "/\n+/" ), "\n", $url );
		$url = trim( preg_replace( "/(\#.+(\n|$))/u", '', $url ) );
		$url = @str_replace( "\n", ',', $url );
		}

//	Else, if we are using the Query-String: Comma-separate the URLs in the same key.
	elseif ( ! @empty( $_GET['get_rss'] ) ) {
		$url = htmlspecialchars( strip_tags( urldecode( $_GET['get_rss'] ) ) );
		}

//	Otherwise, we'll just have to use our own. (Maybe later?)
	else {
//		$url = htmlspecialchars( "https://{$_SERVER['SERVER_NAME']}/feed" );
		$url = '';
		}


	if ( strpos( $url, ',' ) !== false ) {
		$urls = explode( ',', trim( $url ) );

		$i = 0;

		foreach ( $urls as $url ) {

			if ( $i < $channels ) {
				get_feeds( trim( $url ) );
				}

			$i++;
			}
		}

	elseif ( ! empty( $url ) ) {
		get_feeds( $url );
		}

	else {
		echo "<h2 class='rss-error'>Sorry! No usable RSS URLs found.</h2>";
		}

	}


echo <<<HTML

</body>
</html>

HTML;

