<?php

//	Remove unsafe attributes if we are allowing even only a few selected HTML tags!
function remove_attr( $data ) {
	$data = preg_replace( '/(\ +(style|(on|(add|capture|remove)event)[a-z0-9]+)=("|\')[^"\']+(\'|"))/i', ' class="bad-attribute"', $data );
	$data = preg_replace( '/(\ +(href|src)=("|\')\ *((javascript|phpinput|ftp):[^"\']+)(\'|"))/i', ' class="bad-href"', $data );

	return $data;
	}

//	Retrieve a list of feeds from a selected RSS "channel", and send to the User-Agent in a formatted page.
function get_feeds( $url ) {
	global $list_length, $gist_size;
?>

<div class="content widget widget_rss">

<?php
	$invalidurl = false;

	$feeds = @simplexml_load_file( $url );

	if ( ! $feeds ) {
		$invalidurl = true;
		$feeds = '';

		echo "<h2 class='rss-error'>'$url' is an invalid RSS feed URL!</h2>";
		}

	$i = 0;

	if ( ! empty( $feeds ) ) {
		$favicon = parse_url( $feeds->channel->link );

//		echo "<h2 id='rss-site-title'><a id='rss-site-link' href='" . $feeds->channel->link . "'>" . $feeds->channel->title . "</a></h2>\r\n";
//		echo "<img src='{$favicon['scheme']}://{$favicon['host']}/favicon.ico'/>\r\n";

		echo "\t<h2 class='widget-title'><img src='{$favicon['scheme']}://{$favicon['host']}/favicon.ico' width='24'>&nbsp;<a class='rsswidget' href='" . $feeds->channel->link . "'>" . $feeds->channel->title . "</a></h2>\r\n";

		echo "\t<h4 class='rss-channel-desc'>" . $feeds->channel->description . "</h4>\r\n\t<hr>\r\n";

		foreach ( $feeds->channel->item as $item ) {

			$desc = $item->description;
			$other_img_url = '';

//			Find the first embedded image to display, if there isn't one defined as a featured image.
			if ( preg_match( '@(<img[^>]+>)@iu', $desc, $matches ) ) {
				$other_img_url = $matches[0];

				if ( is_array( $other_img_url ) ) {
					$other_img_url = $other_img_url[0];
					}
				}
?>

<div class="post">

	<div class="post-head">
		<h3 class="feed_title"><a class="rsswidget" href="<?php echo $item->link; ?>"><?php echo $item->title; ?></a></h3>
		<h5 class='rss-date'><?php echo date( 'D, M d, Y - H:i', strtotime( $item->pubDate ) ); ?></h5>
<?php 		if ( ! @empty( $item->image->url ) ) { ?>
		<img class="rss-thumbnail" src="<?php echo (string)$item->image->url; ?>">
<?php 		} elseif ( ! empty( $other_img_url ) ) {
				echo "\t\t$other_img_url\r\n";
				} ?>
	</div><!-- .post-head -->

	<div class="post-content">
		<?php

			if ( ! defined( 'NO_HTML' ) || NO_HTML === true ) {
//				$desc = "<pre>" . trim( implode( ' ', array_slice( explode( ' ', strip_tags( $desc ) ), 0, $gist_size ) ) ) . " . . .</pre>\r\n";
				$desc = "<p>" . substr( ltrim( strip_tags( $desc ) ), 0, $gist_size ) . " . . .</p>\r\n";
				}

			elseif ( NO_HTML === false ) {
//				$desc = preg_replace( '@<div[^>]+>.+</div>@siu', '', $desc );	//	DEBUGGING/TESTING!

//				Remove XSS/XMLSS problems.
				$desc = preg_replace( array( '@<script[^>]+>.+</script>@si', '@<style[^>]+>.+</style>@si' ), '', $desc );
				$desc = strip_tags( $desc, '<div><a><p><b><i><u><br /><hr />' );
				$desc = remove_attr( $desc );
				}

			echo $desc;

?>		<h4><a class='rsswidget' href="<?php echo $item->link; ?>">There's more!</a></h4>
	</div><!-- .post-content -->

</div><!-- .post -->

<hr />
<?php
			if ( $i >= ( $list_length - 1 ) ) {
				break;
				}

			$i++;
			}
		}

	else {
		if ( ! $invalidurl ) {
			echo "<h2 class='rss-error'>No items found for $url!</h2>";
			}
		}
?>

</div><!-- .content -->

<br />

<?php }

